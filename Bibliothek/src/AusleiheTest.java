
public class AusleiheTest {

	public static void main(String[] args) {
		
		Ausleihe ausleihe1 = new Ausleihe();
		ausleihe1.setAusleihnummer(1);
		
		Buch b1 = new Buch();
		b1.setTitel("Koran");
		b1.setAutor("Gott");
		ausleihe1.setBuch(b1);
		
		Person p1 = new Person();
		p1.setKundennummer("1");
		p1.setVorname("Erik");
		p1.setNachname("Wirth");
		
		Adresse a1 = new Adresse();
		
		a1.setStrasse("Frankfurter Allee 80");
		a1.setPlz(10247);
		a1.setOrt("Berlin");
		
		p1.setAnschrift(a1);
		ausleihe1.setPerson(p1);
		
		
	
		System.out.println(ausleihe1);
	

	}

}
