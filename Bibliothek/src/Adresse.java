
public class Adresse {

	//Attribute
		private String strasse;
		private int plz;
		private String ort;
		
		//Konstruktor
		public Adresse() {
			this.strasse = "unbekannt";
			this.plz = 0;
			this.ort = "unbekannt";
		
		}
		
		//Verwaltungsmethoden (Getter und Setter)
		public String getStrasse() {
			return this.strasse;
		}
			
		public int getPlz() {
				return this.plz;
			
		}
		public String getOrt() {
			return this.ort;
		
	}
		public void setStrasse(String strasse) {
			this.strasse = strasse;
			
		}
			
		public void setPlz(int plz) {
			this.plz = plz;
		}
		
		public void setOrt(String ort) {
			this.ort = ort;
		}
		

		public String toString() {
			return "Strasse = " + this.strasse + "; PLZ = " + this.plz + "; Ort = " + this.ort;
			
		}
		

	}

