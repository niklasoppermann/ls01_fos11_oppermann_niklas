
public class PersonenTest {

	public static void main(String[] args) {
		Person p1 = new Person();
		p1.setKundennummer("1");
		p1.setVorname("Erik");
		p1.setNachname("Wirth");
		
		Adresse a1 = new Adresse();
		
		a1.setStrasse("Frankfurter Allee 80");
		a1.setPlz(10247);
		a1.setOrt("Berlin");
		
		p1.setAnschrift(a1);
		
		
		Person p2 = new Person();
		p2.setKundennummer("2");
		p2.setVorname("Gerwit");
		p2.setNachname("Gaube");
		
        Adresse a2 = new Adresse();
		
		a2.setStrasse("Frankfurter Allee 80");
		a2.setPlz(10247);
		a2.setOrt("Berlin");
		
		p2.setAnschrift(a2);
		
		Person p3 = new Person();
		p3.setKundennummer("3");
		p3.setVorname("Niklas");
		p3.setNachname("Parisi");
		
        Adresse a3 = new Adresse();
		
		a3.setStrasse("Frankfurter Allee 80");
		a3.setPlz(10247);
		a3.setOrt("Berlin");
		
		p3.setAnschrift(a3);
		
	
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
	}

}
