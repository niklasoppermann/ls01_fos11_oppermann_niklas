public class Person {
	//Attribute
	private String kundennummer;
	private String vorname;
	private String nachname;
	private Adresse anschrift;
	
	//Konstruktor
	public Person() {
		this.kundennummer = "unbekannt";
		this.vorname = "unbekannt";
		this.nachname = "unbekannt";
		this.anschrift = new Adresse();
	
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public String getKundenummer() {
		return this.kundennummer;
	}
		
	public String getVorname() {
			return this.vorname;
		
	}
	public String getNachname() {
		return this.nachname;
	
}
	public void setKundennummer(String kundennummer) {
		this.kundennummer = kundennummer;
		
	}
		
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public Adresse getAnschrift() {
		return this.anschrift;
		
	}
	
	public void setAnschrift(Adresse anschrift) {
		this.anschrift = anschrift;
	}

	public String toString() {
		return "Kundennummer = " + this.kundennummer + "; Vorname = " + this.vorname + "; Nachname = " + this.nachname + "; Adresse : " + this.anschrift;
		
	}
	

}
