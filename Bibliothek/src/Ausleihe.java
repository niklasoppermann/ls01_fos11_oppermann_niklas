
public class Ausleihe {
	//Attribute
	private int ausleihnummer;
	private Buch buch;
	private Person person;
	
	//Konstruktor
	public Ausleihe() {
		this.ausleihnummer = 1;
		this.buch = new Buch();
		this.person = new Person();
	
	}
	
	//Verwaltungsmethoden (Getter und Setter)
	public int getAusleihnummer() {
		return this.ausleihnummer;
	}
		
	public Buch getBuch() {
			return this.buch;
		
	}
	public Person getPerson() {
		return this.person;
	
}
	public void setAusleihnummer(int ausleihnummer) {
		this.ausleihnummer = ausleihnummer;
		
	}
		
	public void setBuch(Buch b1) {
		this.buch = b1;
	}
	
	public void setPerson(Person p1) {
		this.person = p1;
	}
	

	public String toString() {
		return "Ausleihnummer = " + this.ausleihnummer + "; Buch = " + this.buch + "; Person = " + this.person;
		
	}
}
