
public class Schüler extends Person{

	//Attribute
	
		private String schülernummer;
		private String klasse;
		
		
	//Konstruktor
		
		public Schüler() {
			this.schülernummer = "Unbekannt";
			this.klasse = "Unbekannt";
//			setName("Unbekannt");
			
		}
		
		public Schüler(String name) {
			super(name);
			this.klasse = "Unbekannt";
		}
		
		public Schüler(String name, String klasse) {
			super (name);
			this.klasse = klasse;
		}
		
		public void setKlasse(String klasse) {
			this.klasse = klasse;
		}
		public String getKlasse() {
			
		}
	//Getter und Setter
			
			public String getSchülernummer() {
				return this.schülernummer;
			}
			public String getKlasse() {
				return this.klasse;
			}
			
			public void setSchülernummer(String schülernummer) {
				this.schülernummer = schülernummer;
			}
			public void setKlasse(String klasse) {
				this.klasse = klasse;
			}
		
}
