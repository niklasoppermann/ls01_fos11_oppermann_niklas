
public class Person {
	
	//Attribute
	
	private String name;
	
	//Konstruktor
	
	public Person() {
		this.name = "Unbekannt";
		
	}
	public Person(String name) {
		this.name = name;
	}
	
		//Getter und Setter
		
		public String getName() {
			return this.name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
	
	

}
